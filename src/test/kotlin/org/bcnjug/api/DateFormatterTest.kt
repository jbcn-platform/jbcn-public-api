package org.bcnjug.api

import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.utils.toMillis
import org.junit.jupiter.api.Test
import java.util.*


class DateFormatterTest {

    @Test
    fun should_convert_morning_date() {
        // when
        var time = toMillis("2019/05/27", "9:20")
        // then
        val date = Date(time).toString()
        println(date)

        assertThat(time).isEqualTo(1558941600000)
        assertThat(date).contains("27").contains("May")
    }

    @Test
    fun should_convert_afternoon_date() {
        // when
        var time = toMillis("2019/05/27", "14:30")
        // then
        val date = Date(time).toString()
        println(date)

        assertThat(time).isEqualTo(1558960200000)
        assertThat(date).contains("27").contains("May")
    }

    @Test
    fun should_convert_12_hours_date_as_pm() {
        // when
        var time = toMillis("2019/05/27", "12:15")
        // then
        val date = Date(time).toString()
        println(date)

        assertThat(time).isEqualTo(1558952100000)
        assertThat(date).contains("27").contains("May")
    }

    // This test depends on the installed JRE locale

    /*
    @Test
    fun should_convert_00_hours_date_as_am() {
        // when
        var time = toMillis("2019/05/27", "00:25")
        // then
        val date = Date(time).toString()
        println(date)

        assertThat(simpleDateFormat.timeZone.displayName).isEqualTo("Central European Time")
        assertThat(time).isEqualTo(1558909500000)
        // this may fail on CI depending on the timezone of it
        // assertThat(date).contains("27").contains("May")
    }
    */

}
