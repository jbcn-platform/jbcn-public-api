package org.bcnjug.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class SessionsControllerTest {

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }


    @Test
    fun should_get_all_sessions() {
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/sessions"), Argument.of(List::class.java))
            .blockingFirst()
            .body()
        // then
        assertThat(res).hasSizeGreaterThan(0)
        res!!.forEach {
            assertSession(it!!)
        }
    }

    private fun assertSession(sessionMap: Any) {
        val session = sessionMap as Map<String, String>
        // not all contain 'blog' and 'twitter'
        assertThat(session.keys).containsAll(listOf(
            "slotId",
            "roomId",
            "roomName",
            "day",
            "fromTime",
            "fromTimeMillis",
            "toTime",
            "toTimeMillis",
            "talk"))
        val talk = session["talk"] as Map<String, Any>
        assertThat(talk.keys).containsAll(listOf(
            "id",
            "title",
            "talkType",
            "track",
            "trackId",
            "lang",
            "audienceLevel",
            "summary",
            "tags",
            "speakers",
            "speakersDetails"))
        val speakers = talk["speakers"] as List<Map<String, String>>
        (speakers).forEach {
            assertThat(it.keys).containsExactlyInAnyOrder("link", "name")
            assertThat((it["link"] as Map<String, String>).keys).containsExactlyInAnyOrder("href", "rel", "title")
        }
        val speakersData = talk["speakersDetails"] as List<Map<String, String>>
        (speakersData).forEach {
            assertThat(it.keys).contains("uuid", "bio", "fullName")
        }
    }

}