package org.bcnjug.api

import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.utils.ConferenceUriFactory
import org.junit.jupiter.api.Test

class ConferenceUriBuilderTest {

    @Test
    fun should_build_basic_conference_uri() {
        // given
        val factory = ConferenceUriFactory("domain", "path")
        val builder = factory.builder()
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://domain/path")
    }

    @Test
    fun should_build_conference_uri_with_only_domain() {
        // given
        val factory = ConferenceUriFactory("domain", "")
        val builder = factory.builder()
        // then
        val uri = builder.build().toString()
        assertThat(uri).isEqualTo("https://domain")
    }

    // TODO should be validated
    @Test
    fun should_build_wrong_uri_when_domain_is_empty() {
        // given
        val factory = ConferenceUriFactory("", "path")
        val builder = factory.builder()
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https:///path")
    }

    @Test
    fun should_build_conference_uri_with_one_section_added() {
        // given
        val factory = ConferenceUriFactory("conf.com", "api/resource")
        val builder = factory.builder()
            .append("first")
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://conf.com/api/resource/first")
    }

    @Test
    fun should_build_conference_uri_with_many_sections_added() {
        // given
        val factory = ConferenceUriFactory("conf.com", "api/resource")
        val builder = factory.builder()
            .append("first")
            .append("second")
            .append("/third")
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://conf.com/api/resource/first/second/third")
    }

    @Test
    fun should_clear_slashes_in_sections() {
        // given
        val factory = ConferenceUriFactory("conf.com", "api/resource")
        val builder = factory.builder()
            .append("/first/")
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://conf.com/api/resource/first")
    }

    @Test
    fun should_clear_trailing_slashes_in_path() {
        // given
        val factory = ConferenceUriFactory("conf.com", "api/resource/")
        val builder = factory.builder()
            .append("/first/")
            .append("second")
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://conf.com/api/resource/first/second")
    }

    // TODO only clears 1 slash
    @Test
    fun should_clear_starting_slashes_in_path() {
        // given
        val factory = ConferenceUriFactory("conf.com", "/api/resource/")
        val builder = factory.builder()
            .append("/first/")
            .append("second")
        // when
        val uri = builder.build().toString()
        // then
        assertThat(uri).isEqualTo("https://conf.com/api/resource/first/second")
    }

}