package org.bcnjug.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class TalksControllerTest {

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_get_all_talks() {
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/talks"), Argument.of(List::class.java))
            .blockingFirst()
            .body()
        // then
        assertThat(res).hasSizeGreaterThan(0)
        res!!.forEach {
            assertTalk(it!!)
        }
    }

    private fun assertTalk(talkMap: Any) {
        val talk = talkMap as Map<String, String>
        // not all contain 'blog' and 'twitter'
        assertThat(talk.keys).containsAll(listOf(
            "id",
            "title",
            "talkType",
            "lang",
            "audienceLevel",
            "summary",
            "tags",
            "speakers",
            "speakersDetails"))
        (talk["speakers"] as List<Map<String, String>>).forEach {
            assertThat(it.keys).containsExactlyInAnyOrder("link", "name")
            assertThat((it["link"] as Map<String, String>).keys).containsExactlyInAnyOrder("href", "rel", "title")
        }
    }

}