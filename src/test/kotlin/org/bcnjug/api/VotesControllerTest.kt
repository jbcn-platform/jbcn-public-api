package org.bcnjug.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.cfpapi.dto.IdResponse
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Vote
import org.junit.Ignore
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import javax.inject.Inject

@MicronautTest
class VotesControllerTest {

    // injection as constructor does not work
    @Inject
    lateinit var api: CfpapiClient

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_add_a_vote_with_minimal_fields() {
        // given
        val vote = Vote(
            talkId = "talkId",
            email = "speaker@domain.com",
            value = 3,
            delivery = null,
            other = null,
            _id = null
        )
        // when
        val res = client.exchange(HttpRequest.POST<Any>("/votes", vote), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        // then
        assertThat(res._id).isNotBlank()
        // cleanup
        api.deleteVote(res._id).blockingGet()
    }

    @Test
    fun should_add_a_vote_with_all_fields_fields() {
        // when
        val res = addVote()
        // then
        assertThat(res._id).isNotBlank()
        // cleanup
        api.deleteVote(res._id!!).blockingGet()
    }

    // validates if exists with (talkId, email)
    @Test
    fun should_add_a_vote_with_PUT_when_it_does_not_exist() {
        // when
        val vote = Vote(
            talkId = "cool-talkId-${UUID.randomUUID()}",
            email = "speaker@domain.com",
            value = 3,
            delivery = "delivery-me",
            other = "other_other",
            _id = null
        )

        val res = client.exchange(HttpRequest.PUT<Any>("/votes", vote), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        vote._id = res._id
        // then
        assertThat(res._id).isNotBlank()
        // cleanup
        api.deleteVote(res._id).blockingGet()
    }

    @Ignore("temporally disabled")
    // @Test
    fun should_update_a_vote_with_PUT_when_it_already_exists() {
        // when
        val vote = addVote()

        val newVote = Vote(
            _id = null,
            talkId = vote.talkId,
            email = vote.email,
            value = 5,
            other = "new other",
            delivery = "new delivery"
        )

        val res = client.exchange(HttpRequest.PUT<Any>("/votes", newVote), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        vote._id = res._id
        // then
        val updatedVote = api.searchVote(newVote.talkId!!, newVote.email!!).blockingGet()[0]
        assertThat(updatedVote._id).isNotBlank()
        assertThat(updatedVote.talkId).isEqualTo(newVote.talkId)
        assertThat(updatedVote.delivery).isEqualTo(newVote.delivery)
        assertThat(updatedVote.other).isEqualTo(newVote.other)
        // cleanup
        api.deleteVote(res._id).blockingGet()
    }

    private fun addVote(): Vote {
        val vote = Vote(
            talkId = "cool-talkId",
            email = "speaker@domain.com",
            value = 3,
            delivery = "delivery-me",
            other = "other_other",
            _id = null
        )

        val res = client.exchange(HttpRequest.POST<Any>("/votes", vote), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        vote._id = res._id

        return vote
    }

    @Test
    fun should_fail_without_control_when_vote_is_not_valid() {
        // given
        val vote = Vote(
            talkId = "talkId",
            email = "speaker@domain.com",
            value = 10,
            delivery = null,
            other = null,
            _id = null
        )
        // when
        val res = client.exchange(HttpRequest.POST<Any>("/votes", vote), IdResponse::class.java)

        assertThrows(HttpClientResponseException::class.java) {
            res.blockingFirst()
        }
    }

    @Test
    fun should_get_vote_by_talkId_and_attendee_email() {
        // given
        val testVote = addVote()
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/votes/${testVote.talkId}/${testVote.email}"), Vote::class.java)
            .blockingFirst()
            .body()!!
        // then
        assertThat(res._id).isNotBlank()
        assertThat(res.talkId).isEqualTo(res.talkId)
        assertThat(res.email).isEqualTo(res.email)
        // cleanup
        api.deleteVote(testVote._id!!).blockingGet()
    }

    // even when the error is trated, micronaut throws an exception, so we need to try-catch it
    @Test
    fun should_return_404_when_getting_a_vote_that_does_not_exist() {
        // when
        try {
            client.exchange(HttpRequest.GET<Any>("/votes/${UUID.randomUUID()}/${UUID.randomUUID()}"))
                .doOnError {
                    // then
                    assertThat((it as HttpClientResponseException).status).isEqualTo(HttpStatus.NOT_FOUND)
                }
                .blockingFirst()
        } catch (e: Exception) {
            assertThat(e).hasMessage("Not Found")
        }
    }

}
