package org.bcnjug.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class SponsorsControllerTest {

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_get_all_sponsors() {
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/sponsors"), Argument.of(List::class.java))
            .blockingFirst()
            .body()
        // then
        assertThat(res).hasSizeGreaterThan(0)
        res!!.forEach {
            assertSponsor((it as Map<String, *>?)!!)
        }
    }

    @Test
    fun should_get_every_sponsor_by_id() {
        val sponsors = client.exchange(HttpRequest.GET<Any>("/sponsors"), Argument.of(List::class.java))
            .blockingFirst()
            .body()!!

        sponsors.forEach {
            val expected = (it as Map<String, String>)
            val id = expected["id"]
            val restSponsor = client.exchange(HttpRequest.GET<Any>("/sponsors/$id"), Argument.of(Map::class.java))
                .blockingFirst()
                .body() as Map<String, Any>
            assertThat(restSponsor).isNotNull
            assertSponsor(restSponsor)
            assertThat(restSponsor["id"]).isEqualTo(id)
        }
    }

    @Test
    fun should_get_every_sponsor_by_code() {
        val sponsors = client.exchange(HttpRequest.GET<Any>("/sponsors"), Argument.of(List::class.java))
            .blockingFirst()
            .body()!!

        sponsors.forEach {
            val expected = (it as Map<String, String>)
            val code = expected["code"]
            val restSponsor = client.exchange(HttpRequest.GET<Any>("/sponsors/code/$code"), Argument.of(Map::class.java))
                .blockingFirst()
                .body() as Map<String, Any>
            assertThat(restSponsor).isNotNull
            assertSponsor(restSponsor)
            assertThat(restSponsor["code"]).isEqualTo(code)
        }
    }

    private fun assertSponsor(sponsorMap: Map<String, *>) {
        // not all contain 'blog' and 'twitter'
        assertThat(sponsorMap.keys).containsAll(listOf(
            "id",
            "name",
            "href",
            "image",
            "level",
            "code"))
        (sponsorMap["image"] as Map<*, *>).keys.containsAll(listOf("src", "alt"))
        (sponsorMap["level"] as Map<*, *>).keys.containsAll(listOf("priority", "name"))
    }

}