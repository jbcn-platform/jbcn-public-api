package org.bcnjug.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest

import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.cfpapi.dto.IdResponse
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.FavoredTalk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap

@MicronautTest
class FavoredControllerTest {

    // injection as constructor does not work
    @Inject
    lateinit var api: CfpapiClient

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_add_a_favored_talk() {
        // given
        val res = addFavored()
        // then
        assertThat(res._id).isNotBlank()
        // cleanup
        api.deleteFavored(res._id!!).blockingGet()
    }

    // "not working, but not used"
    // @Test
    fun should_delete_a_favored_talk_by_id() {
        // given
        val fav = addFavored()
        assertThat(fav._id).isNotBlank()
        // then

        val res = client.exchange(HttpRequest.DELETE<Any>("/favored/${fav._id}", ""), HashMap::class.java)
            .blockingFirst()
            .body()!!
        assertThat(res["status"] as Boolean).isTrue()
    }

    @Test
    fun should_delete_a_favored_talk_by_talkId_and_user() {
        // given
        val fav = addFavored()
        assertThat(fav._id).isNotBlank()
        // then

        val res = client.exchange(HttpRequest.DELETE<Any>("/favored/${fav.user}/remove/${fav.talkId}", ""), HashMap::class.java)
            .blockingFirst()
            .body()!!
        assertThat(res["status"] as Boolean).isTrue()
    }

    @Test
    fun should_retrieve_favored_talks_by_user() {
        // given
        val user = "user-${UUID.randomUUID()}"
        val fav1 = addFavored(user)
        val fav2 = addFavored(user)
        // then
        val res = client.exchange(HttpRequest.GET<Any>("/favored/$user"), HashMap::class.java)
            .blockingFirst()
            .body()!!

        val values = res["favored"] as List<Map<String, String>>
        assertThat(values).hasSize(2)
        // cleanup
        api.deleteFavored(fav1.talkId, fav1.user).blockingGet()
        api.deleteFavored(fav2.talkId, fav2.user).blockingGet()
    }

    @Test
    fun should_retrieve_all_favored_talks_with_count() {
        // given
        val user = "test-user-${UUID.randomUUID()}"
        val talkId = "test-talk-${UUID.randomUUID()}"
        val fav1 = addFavored(user, talkId)
        val fav2 = addFavored(user, talkId)
        // then
        val res = client.exchange(HttpRequest.GET<Any>("/favored"), HashMap::class.java)
            .blockingFirst()
            .body()!!

        val values = res["favorites"] as List<Map<String, Any>>
        assertThat(values).hasSizeGreaterThan(1)

        val createdVote = values.first { it["id"] == talkId }
        assertThat(createdVote["id"]).isEqualTo(talkId)
        assertThat(createdVote["favs"] as Int).isEqualTo(2)

        // cleanup
        api.deleteFavored(fav1.talkId, fav1.user).blockingGet()
        api.deleteFavored(fav2.talkId, fav2.user).blockingGet()
    }

    private fun addFavored(): FavoredTalk {
        val suffix = UUID.randomUUID().toString()
        val talkId = "talk-$suffix"
        val user = "user-$suffix"
        // when
        val res = client.exchange(HttpRequest.POST<Any>("/favored/$user/add/$talkId", ""), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        return FavoredTalk(res._id, talkId, null, user)
    }

    private fun addFavored(user: String): FavoredTalk {
        val suffix = UUID.randomUUID().toString()
        val talkId = "talk-$suffix"
        // when
        val res = client.exchange(HttpRequest.POST<Any>("/favored/$user/add/$talkId", ""), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        return FavoredTalk(res._id, talkId, null, user)
    }

    private fun addFavored(user: String, talkId: String): FavoredTalk {
        // when
        val res = client.exchange(HttpRequest.POST<Any>("/favored/$user/add/$talkId", ""), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        return FavoredTalk(res._id, talkId, null, user)
    }

}