package org.bcnjug.api.cfpapi

import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class CfpSessionHandlerTest {

    @Inject
    lateinit var sessionHandler: CfpSessionHander

    @Test
    fun should_get_token() {
        // given
        // when
        val token = sessionHandler.getToken().blockingGet()
        // then
        assertThat(token).hasSize(153)
    }


}