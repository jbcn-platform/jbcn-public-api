package org.bcnjug.api

import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.cfpapi.dto.IdResponse
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.cfpapi.model.Gender
import org.bcnjug.api.model.Badge
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@MicronautTest
class BadgesControllerTest {

    // injection as constructor does not work
    @Inject
    lateinit var api: CfpapiClient

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    companion object {
        const val SPONSOR_ID = "99999"
    }

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_add_a_badge() {
        // when
        val res = addBadge(SPONSOR_ID)
        assertThat(res._id).isNotBlank()
        // cleanup
        api.deleteBadge(res._id).blockingGet()
    }

    @Test
    fun should_update_a_badge_details() {
        // given
        val testBadge = addBadge(SPONSOR_ID)
        // when
        val details = "new-details-${UUID.randomUUID()}"
        val res = updateBadge(testBadge._id, details)
        // then
        val updatedBadge = api.getBadge(testBadge._id).blockingGet()

        assertThat(res._id).isNotBlank()
        assertThat(updatedBadge.details).contains(details)
        // cleanup
        api.deleteBadge(res._id).blockingGet()
    }

    @Test
    fun should_delete_a_single_badge() {
        // given
        val badgeId = addBadge(SPONSOR_ID)
        // then
        client.exchange(HttpRequest.DELETE<Any>("/badges/${badgeId._id}"))
            .blockingFirst()
            .body()!!
    }

    @Test
    fun should_retrieve_a_badge_by_sponsor_id() {
        // given
        addBadge(SPONSOR_ID)
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/badges/$SPONSOR_ID"), ArrayList::class.java)
            .blockingFirst()
            .body()!!

        assertThat(res).hasSizeGreaterThan(0)
        // cleanup
        res.forEach {
            api.deleteBadge((it as Map<String, String>)["id"]!!).blockingGet()
        }
    }

    @Test
    fun should_return_empty_badge_list_for_non_existent_sponsor_id() {
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/badges/${UUID.randomUUID()}"), ArrayList::class.java)
            .blockingFirst()
            .body()!!
        // then
        assertThat(res).hasSize(0)
        // cleanup
        res.forEach {
            api.deleteBadge((it as Map<String, String>)["id"]!!).blockingGet()
        }
    }

    private fun addBadge(sponsorId: String): IdResponse {
        val badge = Badge(
            id = null,
            sponsorId = sponsorId,
            name = "test_attendee",
            email = "attendee@dot.com",
            language = "ca",
            age = 99,
            gender = Gender.M.toString(),
            company = "company",
            city = "city",
            country = "country",
            programmingLanguages = "Groovy,Rust",
            jobTitle = "jobTitle",
            details = "created for internal testing",
            timestamp = "1234567890"
        )
        // when
        val res = client.exchange(HttpRequest.PUT<Any>("/badges", badge), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        return res
    }

    private fun updateBadge(badgeId: String, details: String): IdResponse {
        val badge = Badge(
            id = badgeId,
            // note: only details is updatable. We can put any values in the rest
            sponsorId = "123",
            name = "test_attendee",
            email = "attendee@dot.com",
            language = "ca",
            age = 99,
            gender = Gender.F.toString(),
            company = "company",
            city = "city",
            country = "country",
            programmingLanguages = "Groovy,Rust",
            jobTitle = "jobTitle",
            details = "created for internal testing: $details",
            timestamp = "1234567890"
        )
        // when
        val res = client.exchange(HttpRequest.PUT<Any>("/badges", badge), IdResponse::class.java)
            .blockingFirst()
            .body()!!
        return res
    }

}