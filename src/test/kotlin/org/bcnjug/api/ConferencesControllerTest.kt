package org.bcnjug.api

import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.model.Conference
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class ConferencesControllerTest {

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }


    @Test
    fun should_get_conference_information() {
        // when
        val res = client.exchange(HttpRequest.GET<Any>("/conferences/1234"), Argument.of(Conference::class.java))
            .blockingFirst()
            .body()!!
        // then
        assertThat(res.id).isEqualTo("1")
        assertThat(res.name).isNotBlank()
        assertThat(res.website).isNotBlank()
        assertThat(res.imageURL).isNotBlank()

        assertThat(res.fromDate).isNotBlank()
        assertThat(res.endDate).isNotBlank()

        assertThat(res.eventType).isNotBlank()
        assertThat(res.cfpURL).isNotBlank()
        assertThat(res.cfpVersion).isNotBlank()

        assertThat(res.locationId).isEqualTo(1)
        assertThat(res.myBadgeActive).isEqualTo(true)
        assertThat(res.sessionTypes).hasSize(2)
        assertThat(res.floorPlans).hasSize(1)
        assertThat(res.tracks).hasSizeGreaterThan(0)
        assertThat(res.tags).hasSizeGreaterThan(0)
    }

}