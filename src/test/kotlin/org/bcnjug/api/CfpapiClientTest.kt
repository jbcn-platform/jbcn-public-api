package org.bcnjug.api

import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.bcnjug.api.cfpapi.model.*
import org.bcnjug.api.model.Vote
import org.junit.jupiter.api.Test
import java.util.*
import javax.inject.Inject

@MicronautTest
class CfpapiClientTest {

    @Inject
    lateinit var client: CfpapiClient

    @Test
    fun should_get_all_speakers() {
        // when
        val speakers = client.getAllSpeakers()

        // then
        assertThat(speakers).isNotNull
        val result = speakers.blockingGet()
        assertThat(result).hasSizeGreaterThan(10)
        result.forEach {
            assertThat(it.ref).isNotEmpty()
            assertThat(it.fullName).isNotEmpty()
            assertThat(it.biography).isNotEmpty()
            assertThat(it.company).isNotEmpty()
            assertThat(it.email).isNotEmpty()
        }
    }

    @Test
    fun should_get_speaker_by_ref() {
        // given
        val realRef = "6c31223d3be1de9ea66a96974ec4f524cd6a786f"
        // when
        val maybe = client.getSpeaker(realRef)
        // then
        assertThat(maybe).isNotNull
        val speaker = maybe.blockingGet()

        assertThat(speaker.ref).isNotEmpty()
        assertThat(speaker.fullName).isNotEmpty()
        assertThat(speaker.biography).isNotEmpty()
        assertThat(speaker.company).isNotEmpty()
    }

    @Test
    fun should_get_all_talks() {
        // when
        val talks = client.getAllTalks()
        // then
        assertThat(talks).isNotNull
        val result = talks.blockingGet()
        assertThat(result).hasSizeGreaterThan(1)
        result.forEach {
            assertThat(it._id).isNotEmpty()
            assertThat(it.title).isNotEmpty()
            assertThat(it.paperAbstract).isNotEmpty()
            assertThat(it.type).isNotEmpty()
            assertThat(it.tags).hasSizeGreaterThan(0)
            assertThat(it.level).isNotEmpty()
            assertThat(it.speakers).hasSizeGreaterThan(0)
        }
    }

    @Test
    fun should_get_all_sessions() {
        // when
        val rooms = client.getAllSessions()
        // then
        assertThat(rooms).isNotNull
        val result = rooms.blockingGet()

        assertThat(result).hasSizeGreaterThan(0)
        assertThat(result.get(0)._id).isNotEmpty()
        assertThat(result.get(0).day).isNotEmpty()
        assertThat(result.get(0).startTime).isNotEmpty()
        assertThat(result.get(0).endTime).isNotEmpty()
        assertThat(result.get(0).trackId).isNotNull()
    }

    @Test
    fun should_get_all_rooms() {
        // when
        val rooms = client.getAllRooms()
        // then
        assertThat(rooms).isNotNull
        val result = rooms.blockingGet()
        assertThat(result).hasSizeGreaterThan(0)
        assertThat(result.get(0)._id).isNotEmpty()
        assertThat(result.get(0).name).isNotEmpty()
    }

    @Test
    fun should_get_all_tracks() {
        // when
        val tracks = client.getAllTracks()
        // then
        assertThat(tracks).isNotNull
        val result = tracks.blockingGet()
        assertThat(result).hasSizeGreaterThan(0)
        assertThat(result.get(0)._id).isNotEmpty()
        assertThat(result.get(0).name).isNotEmpty()
        if (result.get(0).description != null)
            assertThat(result.get(0).description).isNotEmpty()
    }

    @Test
    fun should_vote_talk_with_minimal_fields() {
        // given
        val suffix = UUID.randomUUID().toString()
        val vote = Vote(
            talkId = "1234567890-$suffix",
            email = "user-$suffix@email.com",
            value = 5,
            delivery = null,
            other = null,
            _id = null
        )

        // when
        val voteId = client.voteTalk(vote).blockingGet()
        // then
        assertThat(voteId).isNotNull()
    }

    @Test
    fun should_vote_talk_with_all_fields() {
        // given
        val suffix = UUID.randomUUID().toString()
        val vote = Vote(
            talkId = "1234567890-$suffix",
            email = "user-$suffix@email.com",
            value = 5,
            delivery = "delivery-$suffix",
            other = "other-$suffix",
            _id = null
        )

        // when
        val voteId = client.voteTalk(vote).blockingGet()
        // then
        assertThat(voteId).isNotBlank()
    }

    @Test
    fun should_delete_a_vote() {
        // given
        val suffix = UUID.randomUUID().toString()
        val vote = Vote(
            talkId = "1234567890-$suffix",
            email = "user-$suffix@email.com",
            value = 5,
            delivery = null,
            other = null,
            _id = null
        )
        val voteId = client.voteTalk(vote).blockingGet()
        // when
        val response = client.deleteVote(voteId).blockingGet()
        // then
        assertThat(response).isEqualTo(true)
    }

    @Test
    fun should_get_sponsor_by_id() {
        // given
        val sponsorId = "1"
        // when
        val maybe = client.getSponsor(sponsorId)
        // then
        assertThat(maybe).isNotNull
        val speaker = maybe.blockingGet()

        assertThat(speaker.id).isNotEmpty()
        assertThat(speaker.name).isNotEmpty()
        assertThat(speaker.code).isNotEmpty()
        assertThat(speaker.href).isNotEmpty()
        assertThat(speaker.image).isNotNull
        assertThat(speaker.image!!.src).isNotEmpty()
        assertThat(speaker.image!!.alt).isNotEmpty()
        assertThat(speaker.level).isNotNull
        assertThat(speaker.level!!.priority).isGreaterThan(0)
        assertThat(speaker.level!!.name).isNotEmpty()
    }

    @Test
    fun should_get_talk_tags() {
        // when
        val tags = client.getAllTags().blockingGet()
        // then
        assertThat(tags).hasSizeGreaterThan(0)
    }

    @Test
    fun should_add_badge_with_all_fields() {
        // given
        val suffix = UUID.randomUUID().toString()
        val vote = testBadge(suffix)

        // when
        val badgeId = client.upsertBadge(vote).blockingGet()
        // then
        assertThat(badgeId).isNotBlank()
        // cleanup
        client.deleteBadge(badgeId).blockingGet()
    }

    @Test
    fun should_get_badges_by_sponsor_code() {
        // given
        val suffix = UUID.randomUUID().toString()
        val testBadge = testBadge(suffix)
        val badgeId = client.upsertBadge(testBadge).blockingGet()
        // when
        val badges = client.searchBadges("sponsor.code" to testBadge.sponsor.code!!).blockingGet()
        // then
        assertThat(badges).hasSizeGreaterThan(0)
        // cleanup
        client.deleteBadge(badgeId).blockingGet()
    }

    private fun testBadge(suffix: String): CfpBadge {
        return CfpBadge(
            _id = null,
            attendee = CfpAttendee(
                name = "attendee",
                email = "email-$suffix@dot.com",
                language = "ca, es",
                age = 44,
                gender = Gender.M.toString(),
                company = "myself",
                jobTitle = "World dominator",
                city = "Bcn",
                country = "Supein",
                programmingLanguages = "Groovy, Typescript"
            ),
            sponsor = CfpSponsor(
                id = "99999",
                name = "sponsor-name-$suffix",
                code = "sponsor-code-$suffix",
                href = null,
                image = null,
                level = null
            ),
            details = "some details go here: $suffix",
            createdDate = null
        )
    }

}
