package org.bcnjug.api

import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.http.client.RxHttpClient
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import javax.inject.Inject

@MicronautTest
class SpeakersControllerTest {

    @Inject
    lateinit var server: EmbeddedServer

    lateinit var client: RxHttpClient

    companion object {
        val mapper = ObjectMapper()
    }


    @BeforeEach
    fun before() {
        client = server.applicationContext.createBean(RxHttpClient::class.java, server.url)
    }

    @Test
    fun should_get_all_speakers() {
        // when
        val res = client.retrieve("/speakers").blockingFirst()
        // then
        assertThat(res).isNotNull()

        val readValue = mapper.readValue(res, Array<Any>::class.java)
        assertThat(readValue).hasSizeGreaterThan(0)
        readValue.forEach { assertSpeaker(it) }
    }

    @Test
    fun should_get_speaker_by_id() {
        // given
        val id = "6c31223d3be1de9ea66a96974ec4f524cd6a786f"
        // when
        val res = client.retrieve("/speakers/$id").blockingFirst()
        // then
        assertThat(res).isNotNull()
        assertSpeaker(mapper.readValue(res, java.util.LinkedHashMap::class.java))
    }

    private fun assertSpeaker(it: Any) {
        val speaker = it as Map<String, String>
        // not all contain 'blog' and 'twitter'
        assertThat(speaker.keys).containsAll(listOf(
            "uuid",
            "bio",
            "firstName",
            "lastName",
            "avatarURL",
            "company",
            "lang",
            "acceptedTalks",
            "fullName",
            "summary",
            "links"
        ))
        (speaker["acceptedTalks"] as List<Map<String, String>>).forEach { talk ->
            // TODO missing: trackId, track
            assertThat(talk.keys).containsAll(listOf(
                "id",
                "title",
                "published",
                "talkType",
                "lang",
                "audienceLevel",
                "summary",
                "tags",
                "speakers",
                "speakersDetails"
            ))
            if (talk["talkType"] == " workshop")
                assertThat(talk.keys).containsAll(listOf("requiremenents"))

        }
        (speaker["links"] as List<Map<String, String>>).forEach { talk ->
            assertThat(talk.keys).containsExactlyInAnyOrder("href", "rel", "title")
        }
    }
}