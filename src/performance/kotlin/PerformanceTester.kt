import org.abelsromero.parallels.jobs.ParallelExecutor
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse
import java.net.http.HttpResponse.BodyHandlers
import java.nio.charset.StandardCharsets


fun main() {

//    val endpoint = "https://jbcnapi.cleverapps.io"
    val endpoint = "http://localhost:8080"

    println("== Testing against: $endpoint")

    repeat(1) {
        // runTest { createTestVote("$endpoint/votes") }
        runTest { fetchUrl("$endpoint/speakers/") }
        runTest { fetchUrl("$endpoint/sessions/") }
        println("----------------------------------------------------------")
    }

}

fun runTest(callable: (Unit) -> HttpResponse<String>) {
    val executionsCount = 1000
    val threadCount = 20
    val executor = ParallelExecutor(threadCount, executionsCount)


    val results = executor.run {
        callable.invoke(Unit).let {
            val success = (it.statusCode() == 200 && !it.body().isNullOrEmpty())
            if (!success)
                println(it.body())

            success
        }
    }

    println("Execution time (ms): ${results.getTime()}")
    println("Ratio (ops/sec): ${results.getJobsPerSecond()}")
    println("Successful ops: ${results.getSuccessfulJobs().getCount()}")
    println("Successful Avg. time (millis): ${results.successfulJobs.avgTime}")
    println("Successful min time (millis): ${results.successfulJobs.minTime}")
    println("Successful max time (millis): ${results.successfulJobs.maxTime}")
    println("Failed ops: ${results.getFailedJobs().getCount()}")
    println("Failed Avg. time (millis): ${results.getFailedJobs().getAvgTime()}")

}

fun fetchUrl(url: String, printResponse: Boolean = false): HttpResponse<String> {
    val httpClient = httpClientV1()
    val request = HttpRequest.newBuilder()
        .uri(URI.create(url))
        .GET()
        .build()

    val response = httpClient.send(request, BodyHandlers.ofString())
    if (printResponse)
        println(response.body())
    return response
}

fun createTestVote(url: String, printResponse: Boolean = false): HttpResponse<String> {

    val httpClient = httpClientV1()
    val ofString = HttpRequest.BodyPublishers.ofString("""{
  "talkId": "5b0f095838da163c1bd328c6",
  "email": "fake_user@fake.com",
  "value": 5,
  "delivery":"internal testing"
}""", StandardCharsets.UTF_8)

    val request = HttpRequest.newBuilder()
        .uri(URI.create(url))
        .POST(ofString)
        .header("Content-Type", "application/json")
        .build()

    val response = httpClient.send(request, BodyHandlers.ofString())
    if (printResponse)
        println(response.body())
    return response
}

private fun httpClientV1(): HttpClient {
    return HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_1_1)
        .build()
}

private fun httpClientV2(): HttpClient {
    return HttpClient.newBuilder()
        .version(HttpClient.Version.HTTP_2)
        .build()
}
