package org.bcnjug.api.cfpapi.model

enum class Gender {
    M, F
}

data class CfpAttendee(
    val name: String,
    val email: String,
    var language: String?,
    var age: Int?,
    var gender: String?,

    var company: String?,
    var jobTitle: String?,
    var city: String?,
    var country: String?,
    var programmingLanguages: String?
)