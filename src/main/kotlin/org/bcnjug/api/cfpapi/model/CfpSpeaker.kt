package org.bcnjug.api.cfpapi.model

data class CfpSpeaker(

    var ref: String,

    var fullName: String,
    var email: String,
    var jobTitle: String,
    var company: String,

    var biography: String,
    var picture: String,
    var publicPicture: String?,

    var web: String?,
    var twitter: String?,
    var linkedin: String?

)