package org.bcnjug.api.cfpapi.model

data class CfpTrack(
    val _id: String,
    val name: String,
    val description: String?
)