package org.bcnjug.api.cfpapi.model

data class CfpRoom(
    val _id: String,
    val name: String,
    val capacity: Int?
)