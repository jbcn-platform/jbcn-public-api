package org.bcnjug.api.cfpapi.model

import com.fasterxml.jackson.annotation.JsonProperty

data class CfpTalk(
    val _id: String,
    var title: String,
    var published: Boolean,
    var sessionId: String?,
    @JsonProperty("paperAbstract")
    var paperAbstract: String,
    var type: String,
    var tags: Array<String>,
    var level: String,
    var speakers: Array<CfpSpeaker>
)