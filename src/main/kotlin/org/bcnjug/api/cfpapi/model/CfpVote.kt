package org.bcnjug.api.cfpapi.model

data class CfpVote(
    var _id: String?,
    val talkId: String,
    val userEmail: String,
    val value: Int,
    val delivery: String?,
    val other: String?,
    val createdDate: String?
)