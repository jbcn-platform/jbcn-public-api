package org.bcnjug.api.cfpapi.model

data class CfpSponsor(
    val id: String,
    val name: String?,
    val href: String?,
    val image: CfpSponsorImge?,
    val level: CfpSponsorLevel?,
    val code: String?
)

data class CfpSponsorImge(
    val src: String?,
    val alt: String?
)


data class CfpSponsorLevel(
    val priority: Int,
    val name: String?
)