package org.bcnjug.api.cfpapi.model

data class CfpSession(
    val _id: String,
    val slotId: String,
    val roomId: String,
    var trackId: String,
    var day: String,
    var startTime: String,
    var endTime: String
)