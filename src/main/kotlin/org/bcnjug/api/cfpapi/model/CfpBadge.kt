package org.bcnjug.api.cfpapi.model

data class CfpBadge(
    val _id: String?,
    val attendee: CfpAttendee,
    val sponsor: CfpSponsor,
    var details: String?,
    val createdDate: String?
)