package org.bcnjug.api.cfpapi.dto

class MapList(
    val items: List<MutableMap<String,Any>>,
    val size: Int?
)