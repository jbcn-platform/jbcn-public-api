package org.bcnjug.api.cfpapi.dto

import org.bcnjug.api.cfpapi.model.CfpSession

data class CfpSessionsList(
    val items: List<CfpSession>,
    val total: Int?
)