package org.bcnjug.api.cfpapi.dto

import org.bcnjug.api.cfpapi.model.CfpSpeaker

data class CfpSpeakersList(
    val items: List<CfpSpeaker>,
    val size: Int?
)