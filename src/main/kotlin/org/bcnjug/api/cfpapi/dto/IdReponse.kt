package org.bcnjug.api.cfpapi.dto

data class IdResponse(val _id: String)