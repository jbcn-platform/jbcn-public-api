package org.bcnjug.api.cfpapi.dto

class SearchResult<T>(
    val items: List<T>,
    val size: Int?
)