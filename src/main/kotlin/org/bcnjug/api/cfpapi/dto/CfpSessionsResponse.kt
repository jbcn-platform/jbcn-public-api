package org.bcnjug.api.cfpapi.dto

data class CfpSessionsResponse(
    val status: Boolean,
    val data: CfpSessionsList
)