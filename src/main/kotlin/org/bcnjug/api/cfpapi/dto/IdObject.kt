package org.bcnjug.api.cfpapi.dto

data class IdObject(val id: String)