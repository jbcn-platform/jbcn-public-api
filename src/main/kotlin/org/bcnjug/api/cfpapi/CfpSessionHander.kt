package org.bcnjug.api.cfpapi

import io.micronaut.context.annotation.Value
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.reactivex.Maybe
import org.slf4j.LoggerFactory
import javax.inject.Singleton

@Singleton
class CfpSessionHander(
    @Client("https://cfpapi.jbcnconf.com")
    val httpClient: RxHttpClient,
    @Value("\${jbcn.clients.cfp.username}")
    val username: String,
    @Value("\${jbcn.clients.cfp.password}")
    val password: String) {

    // NOTE: https://medium.com/devnibbles/rxjava-the-first-3-patterns-4c112a85b689 see section about caches for improvements
    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(CfpSessionHander::class.java)
    }

    // TODO use Maybe.empty()
    private var token: Maybe<String>? = null
    private var lastFetch: Long = 0

    fun getToken(): Maybe<String> {

        // we are aware this may enter several times concurrently but this is not an issue cause in the end token is assigned at some point
        // and overriding it's value is not a problem
        if (token == null || tokenIsExpired()) {

            logger.info("Retrieving new auth token")

            val post = HttpRequest.POST<Any>("/login", hashMapOf("username" to username, "password" to password))
            lastFetch = System.currentTimeMillis()
            token = httpClient.retrieve(post, Argument.of(HashMap::class.java))
                .firstElement()
                .map {
                    // logger.info("This is called anyway ${post}")
                    (it["data"] as Map<String, String>)["token"]
                }
        }
        return token!!
    }

    // We consider a token expires after 59 minutes
    private fun tokenIsExpired(): Boolean {
        return (lastFetch + (60 * 59 * 1000)) < System.currentTimeMillis()
    }

}
