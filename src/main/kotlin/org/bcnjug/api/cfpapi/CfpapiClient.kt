package org.bcnjug.api.cfpapi.model

import com.fasterxml.jackson.databind.ObjectMapper
import io.micronaut.core.type.Argument
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.CfpSessionHander
import org.bcnjug.api.model.FavoredTalk
import org.bcnjug.api.model.Tag
import org.bcnjug.api.model.Vote
import org.bcnjug.api.rest.SpeakersController
import org.slf4j.LoggerFactory
import javax.inject.Singleton

@Singleton
class CfpapiClient(
    @Client("https://cfpapi.jbcnconf.com")
    val httpClient: RxHttpClient,
    val sessionHandler: CfpSessionHander,
    val mapper: ObjectMapper
) {

    companion object {

        @JvmStatic
        private val logger = LoggerFactory.getLogger(CfpapiClient::class.java)

        const val SEARCH_LIMIT = 1000
    }

    fun getAllSpeakers(): Maybe<List<CfpSpeaker>> {

        return sessionHandler.getToken()
            .flatMap { token ->
                val getRequest = HttpRequest.GET<Any>("/api/speakers")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(getRequest, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { speaker ->
                            mapper.convertValue(speaker, CfpSpeaker::class.java)
                        }
                    }
            }
    }

    fun getSpeaker(ref: String): Maybe<CfpSpeaker> {
        return getAllSpeakers()
            .map { list ->
                list.first { it.ref == ref }
            }
    }

    fun getSponsor(id: String): Maybe<CfpSponsor> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val req = HttpRequest.GET<Any>("/api/sponsors/" + id)
                    .header("Authorization", "Bearer $token")
                httpClient.retrieve(req, HashMap::class.java)
                    .firstElement()
                    .map {
                        extractInstance(it).let { s -> mapper.convertValue(s, CfpSponsor::class.java) }
                    }
            }
    }

    fun getSponsorByCode(code: String): Maybe<CfpSponsor> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val req = HttpRequest.GET<Any>("/api/sponsors?code=" + code)
                    .header("Authorization", "Bearer $token")
                httpClient.retrieve(req, HashMap::class.java)
                    .firstElement()
                    .map {
                        extractItems(it).first().let { s -> mapper.convertValue(s, CfpSponsor::class.java) }
                    }
            }
    }

    private fun extractInstance(it: java.util.HashMap<*, *>): Map<String, String> =
        (it["data"] as Map<String, String>)

    fun getAllTalks(): Maybe<List<CfpTalk>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val req = HttpRequest.POST<Any>("/api/talk/search", hashMapOf(
                    "filters" to arrayOf<Any>(),
                    "sort" to "createdDate",
                    "asc" to false,
                    "page" to 0,
                    "size" to 200
                )).header("Authorization", "Bearer $token")
                httpClient.retrieve(req, HashMap::class.java)
                    .firstElement()
                    .map {
                        extractItems(it)
                            .filter { it["state"] != "canceled" }
                            .map { mapper.convertValue(it, CfpTalk::class.java) }
                    }
            }

    }

    fun getAllSponsors(): Maybe<List<CfpSponsor>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/sponsors")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map {
                            mapper.convertValue(it, CfpSponsor::class.java)
                        }
                    }
            }
    }

    fun getAllSessions(): Maybe<List<CfpSession>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/sessions")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map {
                            mapper.convertValue(it, CfpSession::class.java)
                        }
                    }
            }
    }

    fun getAllRooms(): Maybe<List<CfpRoom>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/rooms")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { v ->
                            CfpRoom(v["_id"]!!, v["name"]!!, if (v.containsKey("capacity") && v["capacity"] != null) v["capacity"]!! as Int else null)
                        }
                    }
            }
    }

    fun getAllTracks(): Maybe<List<CfpTrack>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/tracks")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { v ->
                            CfpTrack(v["_id"]!!, v["name"]!!, if (v.containsKey("description")) v["description"]!! else null)
                        }
                    }
            }
    }

    fun voteTalk(vote: Vote): Maybe<String> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.POST<Any>("/api/attendees/votes/",
                    CfpVote(
                        talkId = vote.talkId!!,
                        userEmail = vote.email!!,
                        value = vote.value!!,
                        delivery = vote.delivery,
                        other = vote.other,
                        _id = null,
                        createdDate = null
                    ))
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        (it["data"] as Map<String, List<Map<String, String>>>)["_id"]!! as String
                    }
            }
    }

    fun upsertVoteTalk(vote: Vote): Maybe<String> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.PUT<Any>("/api/attendees/votes/",
                    CfpVote(
                        talkId = vote.talkId!!,
                        userEmail = vote.email!!,
                        value = vote.value!!,
                        delivery = vote.delivery,
                        other = vote.other,
                        _id = null,
                        createdDate = null
                    ))
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        (it["data"] as Map<String, List<Map<String, String>>>)["_id"]!! as String
                    }
            }
    }

    fun deleteVote(voteId: String): Maybe<Boolean> {
        return delete(voteId, "/api/attendees/votes")
    }

    fun upsertBadge(badge: CfpBadge): Maybe<String> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.PUT<Any>("/api/badges", badge)
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        (it["data"] as Map<String, List<Map<String, String>>>)["_id"] as String
                    }
            }
    }

    fun searchBadges(filter: Pair<String, String>): Maybe<List<CfpBadge>> {
        val search = mapOf(
            "filters" to arrayOf(
                mapOf(
                    "field" to filter.first,
                    "value" to filter.second
                )
            ),
            "page" to mapOf(
                "index" to 0,
                "size" to SEARCH_LIMIT
            ))
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.POST<Any>("/api/badges/search", search)
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { b ->
                            mapper.convertValue(b, CfpBadge::class.java)
                        }
                    }
            }
    }

    fun getBadge(id: String): Maybe<CfpBadge> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val req = HttpRequest.GET<Any>("/api/badges/" + id)
                    .header("Authorization", "Bearer $token")
                httpClient.retrieve(req, HashMap::class.java)
                    .firstElement()
                    .map {
                        extractInstance(it).let { s -> mapper.convertValue(s, CfpBadge::class.java) }
                    }
            }
    }

    fun deleteBadge(id: String): Maybe<Boolean> {
        return delete(id, "/api/badges")
    }

    fun addFavoredTalk(favored: FavoredTalk): Maybe<String> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.POST<Any>("/api/talks/favourites", favored)
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        (it["data"] as Map<String, List<Map<String, String>>>)["_id"] as String
                    }
            }
    }

    fun deleteFavored(_id: String): Maybe<Boolean> {
        return delete(_id, "/api/talks/favourites")
    }

    fun deleteFavored(talkId: String, user: String): Maybe<Boolean> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.DELETE<Any>("/api/talks/favourites?talkId=$talkId&user=$user")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map { it["status"] as Boolean }
            }
    }

    private fun delete(id: String, ctxPath: String): Maybe<Boolean> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.DELETE<Any>("$ctxPath/$id")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map { it["status"] as Boolean }
            }
    }

    fun searchFavoured(user: String): Maybe<List<FavoredTalk>> {
        val search = mapOf(
            "filters" to arrayOf(
                mapOf(
                    "field" to "user",
                    "value" to user
                )
            ),
            "page" to mapOf(
                "index" to 0,
                "size" to SEARCH_LIMIT
            ))
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.POST<Any>("/api/talks/favourites/search", search)
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { b ->
                            mapper.convertValue(b, FavoredTalk::class.java)
                        }
                    }
            }
    }

    fun searchVote(talkId: String, email: String): Maybe<List<CfpVote>> {
        val search = mapOf(
            "filters" to arrayOf(
                mapOf(
                    "field" to "talkId",
                    "value" to talkId
                ),
                mapOf(
                    "field" to "userEmail",
                    "value" to email
                )
            ),
            "page" to mapOf(
                "index" to 0,
                "size" to SEARCH_LIMIT
            ))
        return sessionHandler.getToken()
            .flatMap { token ->
                val request = HttpRequest.POST<Any>("/api/attendees/votes/search", search)
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(request, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { b ->
                            mapper.convertValue(b, CfpVote::class.java)
                        }
                    }
            }
    }

    fun getAllFavourites(): Maybe<List<FavoredTalk>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/talks/favourites")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { v ->
                            mapper.convertValue(v, FavoredTalk::class.java)
                        }
                    }
            }
    }

    private fun extractItems(it: java.util.HashMap<*, *>) =
        (it["data"] as Map<String, List<Map<String, String>>>)["items"]!!

    fun getAllTags(): Maybe<List<Tag>> {
        return sessionHandler.getToken()
            .flatMap { token ->
                val get = HttpRequest.GET<Any>("/api/talks/tags")
                    .header("Authorization", "Bearer $token")

                httpClient.retrieve(get, Argument.of(HashMap::class.java))
                    .firstElement()
                    .map {
                        extractItems(it).map { v ->
                            mapper.convertValue(v, Tag::class.java)
                        }
                    }
            }
    }

}