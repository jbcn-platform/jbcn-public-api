package org.bcnjug.api.utils

import io.micronaut.context.annotation.Value
import io.micronaut.http.uri.UriBuilder
import java.net.URI
import javax.inject.Singleton

@Singleton
class ConferenceUriFactory(
    @Value("\${jbcn.uri.domain}") private val domain: String,
    @Value("\${jbcn.uri.context-path:}") private val path: String
) {

    fun builder(): ConferenceUriBuilder {
        return ConferenceUriBuilder(domain).append(path)
    }

    class ConferenceUriBuilder {

        private val domain: String
        private val pathSections = arrayListOf<String>()

        constructor(domain: String) {
            this.domain = domain
        }

        /**
         * Appends a section to the path
         */
        fun append(section: String): ConferenceUriBuilder {
            pathSections.add(section.removePrefix("/").removeSuffix("/"))
            return this
        }

        fun build(): URI {
            return UriBuilder.of("")
                .scheme("https")
                .host(domain)
                .path(pathSections.filter { it.isNotEmpty() }.let { if (it.isEmpty()) "" else it.joinToString("/", "/") })
                .build()
        }
    }

}
