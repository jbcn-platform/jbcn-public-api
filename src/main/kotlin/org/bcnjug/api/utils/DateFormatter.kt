package org.bcnjug.api.utils

import java.text.SimpleDateFormat
import java.util.*


/**
 * NOTE: defining `simpleDateFormat` as a package attribute causes concurrency issues
 * @param day format '2019/05/27'
 * @param hour format '11:00'
 */
fun toMillis(date: String, time: String): Long {
    val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd HH:mm").apply { timeZone = TimeZone.getTimeZone("Europe/Madrid") }
    return simpleDateFormat.parse("$date $time").time
}
