package org.bcnjug.api.model

data class SpeakerLink(
    val link: Link,
    val name: String
)