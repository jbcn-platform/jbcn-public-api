package org.bcnjug.api.model

data class FloorPlan(
    val id: Int,
    val name: String,
    val imageURL: String
)