package org.bcnjug.api.model

data class Speaker(
    val uuid: String,
    val bio: String,
    val bioAsHtml: String? = null,
    val fullName: String,
    val firstName: String,
    val lastName: String,
    var avatarURL: String,
    var company: String?,
    var blog: String?,
    var twitter: String?,
    var lang: String?,
    var acceptedTalks: List<Talk>?,
    var links: List<Link>
) {

    val summary: String
        get() = bio.replace("\\[(.*?)]\\(.*?\\)".toRegex(), "$1")

}