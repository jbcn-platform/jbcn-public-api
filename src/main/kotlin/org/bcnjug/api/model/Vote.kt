package org.bcnjug.api.model

data class Vote(
    var _id: String?,
    val talkId: String?,
    val email: String?,
    val value: Int?,
    val delivery: String?,
    val other: String?
) {

    companion object {
        fun empty(): Vote {
            return Vote(null, null, null, null, null, null)
        }
    }
}
