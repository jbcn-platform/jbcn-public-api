package org.bcnjug.api.model

data class Session(
    val _id: String,
    val slotId: String,

    var roomId: String,
    var roomName: String,

    var day: String,
    var fromTime: String,
    var fromTimeMillis: Long,
    var toTime: String,
    var toTimeMillis: Long,
    var talk: Talk
)