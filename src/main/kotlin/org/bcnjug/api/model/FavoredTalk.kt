package org.bcnjug.api.model

data class FavoredTalk(
    var _id: String?,

    val talkId: String,
    val talkFavs: Int?,
    val user: String
)
