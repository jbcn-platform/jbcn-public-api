package org.bcnjug.api.model

import org.bcnjug.api.cfpapi.model.CfpTrack

data class Conference(
    val id: String = "1",
    val name: String = "JBCNConf Barcelona 2019",
    val website: String = "https://www.jbcnconf.com/2019/",
    val imageURL: String = "https://www.jbcnconf.com/2019/assets/img/logos/logo-jbcnconf.png",
    val fromDate: String = "2019-05-27",
    val endDate: String = "2019-05-29",
    val eventType: String = "CONFERENCE",
    val cfpURL: String = "https://www.jbcnconf.com/2019/callForPapers.html",
    val cfpVersion: String = "1.0",
    val locationId: Int = 1,
    val myBadgeActive: Boolean = true,
    val sessionTypes: List<SessionType> = listOf(
        SessionType(1, "talk", false),
        SessionType(2, "workshop", false)),
    val floorPlans: List<FloorPlan> = listOf(
        FloorPlan(1, "General map", "http://www.jbcnconf.com/2019/assets/img/venue/venue_maps.png")),
    val tracks: List<CfpTrack>,
    val tags: List<Tag>
)