package org.bcnjug.api.model

data class Talk(
    val id: String,
    val title: String,
    val published: Boolean,
    val talkType: String,
    var track: String?,
    var trackId: String?,
    val lang: String,
    var audienceLevel: String,
    val summary: String,
    var summaryAsHtml: String? = null,
    val requirements: String?,
    var tags: List<Tag>,
    var speakers: List<SpeakerLink>,
    var speakersDetails: List<Speaker>
)
