package org.bcnjug.api.model

data class Tag(val value: String, var count: Int? = null)

