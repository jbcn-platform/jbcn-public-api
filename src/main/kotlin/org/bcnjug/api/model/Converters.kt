package org.bcnjug.api.model

import io.micronaut.context.annotation.Value
import org.bcnjug.api.cfpapi.model.*
import org.bcnjug.api.utils.ConferenceUriFactory
import org.bcnjug.api.utils.toMillis

import javax.inject.Singleton

@Singleton
class Converters(
    private val uriBuilderFactory: ConferenceUriFactory
) {

    @Value("\${jbcn.edition.lang}")
    lateinit var conferenceLang: String

    companion object {
        private val COMPOUND_NAMES = mapOf(
            "Juan Luis" to "Ozáez",
            "Ana Maria" to "Mihalceanu"
        )

        val REQUIREMENTS_URLS = mapOf(
            // Poker
            "5c19651d38da16778cb20fc6" to "https://github.com/wlodekkr/chain-of-responsibility",
            // Istio
            "5c196e0238da165f64820b2a" to "http://bit.ly/istio-workshop-11",
            // Full Stack Reactive with React and Spring WebFlux
            "5c3b3b1938da16698cf41b09" to "https://developer.okta.com/blog/2018/09/24/reactive-apis-with-spring-webflux",
            // Learn Micronaut: a reactive microservices framework for the JVM
            "5c5b576538da160d5d9c8e10" to "https://alvarosanchez.github.io/micronaut-workshop-java/",
            // Kubernetes the AWSome Way!
            "5c6498b438da16483151bb3a" to "https://github.com/oak2278/JBCN-EKS-Workshop/blob/master/README.md",
            // OpenShift for Developers in Action
            "5c770fff38da167fbce02cea" to "http://client-tools-labs.b9ad.pro-us-east-1.openshiftapps.com/workshop/workshop/lab/common-cli",
            // Blockchain Contract Development Workshop for Java Programmers
            "5c94091838da165dcdf15ffd" to "https://github.com/Jelurida/JBCNConf2019/blob/master/JBCNConf.2019.workshop.pdf",
            // Hands-on-lab: Hands-on With Serverless Java
            "5caa61e838da161235721c44" to "https://github.com/prpatel/Serverless-Workshop-Setup-All-Platforms"
        )
    }

    fun speakerFrom(speaker: CfpSpeaker): Speaker {
        val speakerUUID = speaker.ref
        val speakerUri = uriBuilderFactory.builder().append("speakers").append(speakerUUID).build().toString()

        /**
         * IMPORTANT: the speakers with a compound name has it's first and lastname hardcoded
         */

        var first: String
        var last: String

        if (isCompoundName(speaker.fullName)) {
            first = COMPOUND_NAMES.keys.first { speaker.fullName.startsWith(it) }
            last = COMPOUND_NAMES[first]!!
        } else {
            first = speaker.fullName.let { it.substring(0, it.indexOf(" ")) }
            last = speaker.fullName.let { it.substring(it.indexOf(" ") + 1) }
        }

        return Speaker(
            uuid = speakerUUID,
            bio = speaker.biography,
            fullName = speaker.fullName,
            firstName = first,
            lastName = last,
            avatarURL = speaker.publicPicture ?: "https://www.jbcnconf.com/2019/assets/img/logos/logo-jbcnconf.png",
            company = speaker.company,
            blog = speaker.web,
            twitter = speaker.twitter,
            lang = conferenceLang,
            acceptedTalks = null,
            links = listOf(Link(
                href = speakerUri,
                rel = speakerUri,
                title = speaker.fullName
            ))
        )
    }

    private fun isCompoundName(fullName: String): Boolean = COMPOUND_NAMES.keys.any { fullName.startsWith(it) }

    fun talkFrom(talk: CfpTalk): Talk {
        return Talk(
            id = talk._id,
            title = talk.title,
            talkType = talk.type,
            published = talk.published,
            track = null,
            trackId = null,
            lang = conferenceLang,
            audienceLevel = talk.level,
            summary = talk.paperAbstract,
            requirements = REQUIREMENTS_URLS[talk._id],
            tags = talk.tags.map { Tag(it) },
            speakers = talk.speakers.map { speakerLinkFrom(it) },
            speakersDetails = talk.speakers.map { speakerFrom(it) }
        )
    }

    fun sponsorFrom(sponsor: CfpSponsor): Sponsor {
        return Sponsor(
            id = sponsor.id,
            name = sponsor.name,
            href = sponsor.href,
            image = SponsorImge(src = sponsor.image?.src, alt = sponsor.image?.alt),
            level = SponsorLevel(priority = sponsor.level?.priority, name = sponsor.level?.name),
            code = sponsor.code

        )
    }

    private fun speakerLinkFrom(speaker: CfpSpeaker): SpeakerLink {
        return SpeakerLink(
            name = speaker.fullName,
            link = Link(
                href = uriBuilderFactory.builder().append("speakers").append(speaker.ref).build().toString(),
                rel = uriBuilderFactory.builder().append("speakers").build().toString(),
                title = speaker.fullName))
    }

    fun sessionFrom(session: CfpSession, talk: CfpTalk, room: CfpRoom, track: CfpTrack): Session {
        return Session(
            _id = session._id,
            slotId = session.slotId,

            roomId = room._id,
            roomName = room.name,

            day = session.day,
            fromTime = session.startTime,
            fromTimeMillis = toMillis(session.day, session.startTime),
            toTime = session.endTime,
            toTimeMillis = toMillis(session.day, session.endTime),
            talk = talkFrom(talk).apply { trackId = track._id; this.track = track.name }
        )
    }


    fun badgeFrom(it: CfpBadge): Badge {
        return Badge(
            id = it._id,
            sponsorId = it.sponsor.id,
            name = it.attendee.name,
            email = it.attendee.email,
            language = it.attendee.language,
            age = it.attendee.age,
            gender = it.attendee.gender,
            company = it.attendee.company,
            city = it.attendee.city,
            country = it.attendee.country,
            programmingLanguages = if (it.attendee.programmingLanguages != null && it.attendee.programmingLanguages!!.isNotBlank()) it.attendee.programmingLanguages else null,
            jobTitle = it.attendee.jobTitle,
            details = it.details,
            timestamp = it.createdDate
        )
    }

    fun voteFrom(it: CfpVote): Vote {
        return Vote(
            _id = it._id,
            talkId = it.talkId,
            email = it.userEmail,
            delivery = it.delivery,
            other = it.other,
            value = it.value
        )
    }

}