package org.bcnjug.api.model

data class Link(
    val href: String,
    val rel: String,
    val title: String
)
