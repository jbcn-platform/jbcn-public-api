package org.bcnjug.api.model

data class SessionType(
    val id: Int,
    val name: String,
    val pause: Boolean
)