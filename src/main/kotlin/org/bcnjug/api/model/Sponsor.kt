package org.bcnjug.api.model

data class Sponsor(
    val id: String,
    val name: String?,
    val href: String?,
    val image: SponsorImge?,
    val level: SponsorLevel?,
    val code: String?
)

data class SponsorImge(
    val src: String?,
    val alt: String?
)


data class SponsorLevel(
    val priority: Int?,
    val name: String?
)