package org.bcnjug.api.model

data class Badge(
    var id: String?,

    val sponsorId: String,

    val name: String,
    val email: String,
    val language: String?,
    val age: Int?,
    val gender: String?,
    val company: String?,
    val city: String?,
    val country: String?,
    val programmingLanguages: String?,
    val jobTitle: String?,
    val details: String?,
    val timestamp: String?
)
