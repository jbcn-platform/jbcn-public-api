package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Conference
import javax.inject.Inject

@Controller("/conferences")
class ConferenceController(
    @Inject val cfpApi: CfpapiClient
) {

    /**
     *
     * NOTE: id is ignored and most values are hardcoded in Conference class
     **/
    @Get("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun get(@PathVariable id: String): Maybe<HttpResponse<Conference>> {

        return cfpApi.getAllTracks()
            .flatMap { tracks ->
                cfpApi.getAllTags()
                    .map { tags -> Conference(tracks = tracks, tags = tags) }
                    .map { HttpResponse.ok(it) }
            }
    }

}