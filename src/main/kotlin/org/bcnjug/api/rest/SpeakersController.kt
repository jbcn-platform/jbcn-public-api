package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.model.CfpTalk
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Converters
import org.bcnjug.api.model.Speaker
import javax.inject.Inject

@Controller("/speakers")
class SpeakersController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    @Get("/")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): Maybe<HttpResponse<List<Speaker>>> {

        return cfpApi.getAllTalks()
            .flatMap { talks ->
                cfpApi.getAllSpeakers()
                    .map { speakers -> speakers.map { converters.speakerFrom(it) } }
                    .map { speakers ->
                        speakers.map { speaker ->
                            speaker.acceptedTalks = filterTalks(talks, speaker)
                            speaker
                        }
                    }
            }.map { HttpResponse.ok(it) }
    }

    @Get("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun get(@PathVariable id: String): Maybe<HttpResponse<Speaker>> {
        return cfpApi.getAllTalks()
            .flatMap { talks ->
                cfpApi.getSpeaker(id)
                    .map { converters.speakerFrom(it) }
                    .map { speaker ->
                        speaker.acceptedTalks = filterTalks(talks, speaker)
                        speaker
                    }
            }
            .map { HttpResponse.ok(it) }
    }

    private fun filterTalks(talks: List<CfpTalk>, speaker: Speaker) =
        talks
            .filter { it.speakers.any { s -> s.ref == speaker.uuid } }
            .map { converters.talkFrom(it) }

}