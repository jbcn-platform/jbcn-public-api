package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.dto.IdResponse
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Converters
import org.bcnjug.api.model.Vote
import org.slf4j.LoggerFactory
import javax.inject.Inject

@Controller("/votes")
class VotesController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(VotesController::class.java)
    }

    @Post
    @Consumes(MediaType.APPLICATION_JSON)
    fun voteTalk(@Body vote: Vote): Maybe<HttpResponse<IdResponse>> {
        return cfpApi.voteTalk(vote)
            .map { IdResponse(it) }
            .map { HttpResponse.ok(it) }
    }

    @Put
    @Consumes(MediaType.APPLICATION_JSON)
    fun upsertVote(@Body vote: Vote): Maybe<HttpResponse<IdResponse>> {
        return cfpApi.upsertVoteTalk(vote)
            .map { IdResponse(it) }
            .map { HttpResponse.ok(it) }
    }

    @Get("{talkId}/{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getVote(@PathVariable talkId: String, @PathVariable email: String): Maybe<HttpResponse<Vote>> {
        return cfpApi.searchVote(talkId, email)
            .map {
                if (it.isEmpty()) {
                    HttpResponse.notFound()
                } else {
                    logger.info("Found ${it.size} results for (talkId='$talkId' & email='$email')")
                    HttpResponse.ok(converters.voteFrom(it[0]))
                }
            }
    }

}