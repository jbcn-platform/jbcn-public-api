package org.bcnjug.api.rest

import io.micronaut.context.annotation.Value
import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import org.slf4j.LoggerFactory

@Controller("/health")
class HealthController(
    @Value("\${jbcn.version}") private val version: String
    ) {

    companion object {
        @JvmStatic
        private val logger = LoggerFactory.getLogger(HealthController::class.java)
    }


    @Get("/")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): HttpResponse<String> {
        logger.info("Health check invoked")
        return HttpResponse.ok("{\"version\":\"$version\"}")
    }

}