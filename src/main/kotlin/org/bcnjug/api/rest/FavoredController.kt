package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.dto.IdObject
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.FavoredTalk
import javax.inject.Inject

@Controller("/favored")
class FavoredController(
    @Inject val cfpApi: CfpapiClient
) {

    @Post("{user}/add/{talkId}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun addFavored(@PathVariable user: String, @PathVariable talkId: String): Maybe<HttpResponse<HashMap<String, String>>> {

        val favored = FavoredTalk(
            _id = null,
            user = user,
            talkId = talkId,
            talkFavs = null
        )

        return cfpApi.addFavoredTalk(favored)
            .map { hashMapOf("_id" to it) }
            .map { HttpResponse.ok(it) }
    }

    @Get
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): Maybe<HttpResponse<Map<String, List<Map<String, Any?>>>>>? {
        return cfpApi.getAllFavourites()
            .map { favs ->
                favs.distinctBy { it.talkId }
                    .map { mapOf("id" to it.talkId, "favs" to it.talkFavs) }
            }
            .map { mapOf("favorites" to it) }
            .map { HttpResponse.ok(it) }
    }

    @Get("{user}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getByUser(@PathVariable user: String): Maybe<HttpResponse<Map<String, List<IdObject>>>> {
        return cfpApi.searchFavoured(user)
            .map { it.map { f -> IdObject(f.talkId) } }
            .map { mapOf("favored" to it) }
            .map { HttpResponse.ok(it) }
    }

    @Delete("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun delete(@PathVariable id: String): Maybe<HttpResponse<Map<String, Boolean>>> {
        return cfpApi.deleteFavored(id)
            .map { mapOf("status" to it) }
            .map { HttpResponse.ok(it) }
    }

    @Delete("{user}/remove/{talkId}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun delete(@PathVariable user: String, @PathVariable talkId: String): Maybe<HttpResponse<Map<String, Boolean>>> {
        return cfpApi.deleteFavored(talkId, user)
            .map { mapOf("status" to it) }
            .map { HttpResponse.ok(it) }
    }

}