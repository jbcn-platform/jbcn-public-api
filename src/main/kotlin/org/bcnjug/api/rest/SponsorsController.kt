package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.PathVariable
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Converters
import org.bcnjug.api.model.Sponsor
import javax.inject.Inject

@Controller("/sponsors")
class SponsorsController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    @Get("/")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): Maybe<HttpResponse<List<Sponsor>>> {
        return cfpApi.getAllSponsors()
            .map { sponsors -> sponsors.map { converters.sponsorFrom(it) } }
            .map { HttpResponse.ok(it) }
    }

    @Get("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun get(@PathVariable id: String): Maybe<HttpResponse<Sponsor>> {
        return cfpApi.getSponsor(id)
            .map { sponsor -> converters.sponsorFrom(sponsor) }
            .map { HttpResponse.ok(it) }
    }

    @Get("/code/{code}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getByCode(@PathVariable code: String): Maybe<HttpResponse<Sponsor>> {
        return cfpApi.getSponsorByCode(code)
            .map { sponsor -> converters.sponsorFrom(sponsor) }
            .map { HttpResponse.ok(it) }
    }

}