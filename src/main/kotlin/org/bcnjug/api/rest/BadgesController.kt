package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.*
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.dto.IdResponse
import org.bcnjug.api.cfpapi.model.CfpAttendee
import org.bcnjug.api.cfpapi.model.CfpBadge
import org.bcnjug.api.cfpapi.model.CfpSponsor
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Badge
import org.bcnjug.api.model.Converters
import javax.inject.Inject

@Controller("/badges")
class BadgesController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    @Put
    @Consumes(MediaType.APPLICATION_JSON)
    fun addBadge(@Body badge: Badge): Maybe<HttpResponse<IdResponse>> {

        val cfpBadge = CfpBadge(
            _id = badge.id,
            attendee = CfpAttendee(
                name = badge.name,
                email = badge.email,
                language = badge.language,
                age = badge.age,
                gender = badge.gender,
                company = badge.company,
                jobTitle = badge.jobTitle,
                city = badge.city,
                country = badge.country,
                programmingLanguages = badge.programmingLanguages
            ),
            sponsor = CfpSponsor(
                id = badge.sponsorId,
                name = null,
                code = null,
                href = null,
                image = null,
                level = null
            ),
            details = badge.details,
            createdDate = null
        )

        return cfpApi.upsertBadge(cfpBadge)
            .map { IdResponse(it) }
            .map { HttpResponse.ok(it) }
    }

    @Get("{sponsorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun filterBadgesBySponsor(@PathVariable sponsorId: String): Maybe<HttpResponse<List<Badge>>> {
        return cfpApi.searchBadges("sponsor.id" to sponsorId)
            .map { it.map { b -> converters.badgeFrom(b) } }
            .map { HttpResponse.ok(it) }
    }


    @Delete("{badgeId}")
    @Consumes(MediaType.APPLICATION_JSON)
    fun deleteBadges(@PathVariable badgeId: String): Maybe<HttpResponse<Boolean>> {
        return cfpApi.deleteBadge(badgeId)
            .map { HttpResponse.ok(it) }
    }

}