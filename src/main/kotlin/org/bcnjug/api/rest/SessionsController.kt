package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Converters
import org.bcnjug.api.model.Session
import javax.inject.Inject

@Controller("/sessions")
class SessionsController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    @Get("/")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): Maybe<HttpResponse<List<Session>>> {

        return cfpApi.getAllTalks().flatMap { talks ->
            cfpApi.getAllRooms().flatMap { rooms ->
                cfpApi.getAllTracks().flatMap { tracks ->
                    cfpApi.getAllSessions()
                        .map { sessions ->
                            sessions
                                .map { s ->
                                    // may return null if data is not consistent (e.g. room was deleted)
                                    val talk = talks.first { s._id == it.sessionId }
                                    val room = rooms.first { it._id == s.roomId }
                                    val track = tracks.first { it._id == s.trackId }
                                    converters.sessionFrom(s, talk, room, track)
                                }
                                .sortedWith(Comparator { s1, s2 ->
                                    val timeOrder = s1.fromTimeMillis.compareTo(s2.fromTimeMillis)
                                    if (timeOrder == 0) s1.talk.track!!.compareTo(s2.talk.track!!)
                                    else timeOrder
                                })
                        }
                }
            }
        }.map { HttpResponse.ok(it) }
    }

}