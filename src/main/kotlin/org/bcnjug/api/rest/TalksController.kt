package org.bcnjug.api.rest

import io.micronaut.http.HttpResponse
import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Consumes
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.reactivex.Maybe
import org.bcnjug.api.cfpapi.model.CfpapiClient
import org.bcnjug.api.model.Converters
import org.bcnjug.api.model.Talk
import org.slf4j.LoggerFactory
import javax.inject.Inject

@Controller("/talks")
class TalksController(
    @Inject val cfpApi: CfpapiClient,
    @Inject val converters: Converters
) {

    @Get("/")
    @Consumes(MediaType.APPLICATION_JSON)
    fun getAll(): Maybe<HttpResponse<List<Talk>>> {
        return cfpApi.getAllTalks()
            .map { talks -> talks.map { converters.talkFrom(it) } }
            .map { HttpResponse.ok(it) }
    }

}