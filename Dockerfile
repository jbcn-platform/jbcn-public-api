#FROM openjdk:8u191-jdk-alpine3.9
FROM openjdk:11.0.2-jdk-slim-stretch
#RUN apk --no-cache add curl
COPY build/libs/*-all.jar mobile-api.jar
CMD java ${JAVA_OPTS} -jar mobile-api.jar